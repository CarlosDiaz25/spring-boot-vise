CREATE DATABASE `prueba_vise` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE `producto` (
  `prd_keyprd` int NOT NULL AUTO_INCREMENT,
  `prd_nombre` varchar(45) NOT NULL,
  `prd_marcap` varchar(45) NOT NULL,
  `prd_hechon` varchar(45) NOT NULL,
  `prd_precio` float NOT NULL,
  PRIMARY KEY (`prd_keyprd`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
