CREATE DATABASE `prueba_vise`;
use prueba_vise;

CREATE TABLE `marca` (
  `mar_keymar` bigint NOT NULL AUTO_INCREMENT,
  `mar_fecalt` datetime NOT NULL,
  `mar_fecult` datetime NOT NULL,
  `mar_nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`mar_keymar`),
  UNIQUE KEY `UK_keid4ke972aqu25q9y1hxwlmc` (`mar_nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `producto` (
  `prd_keyprd` int NOT NULL AUTO_INCREMENT,
  `prd_nombre` varchar(45) NOT NULL,
  `prd_hechon` varchar(45) NOT NULL,
  `prd_precio` float NOT NULL,
  `prd_fecalt` datetime NOT NULL,
  `prd_fecult` datetime NOT NULL,
  `mar_keymar` bigint DEFAULT NULL,
  PRIMARY KEY (`prd_keyprd`),
  KEY `FKwmots4yihh3ax74p1b0ce66b` (`mar_keymar`),
  CONSTRAINT `FKwmots4yihh3ax74p1b0ce66b` FOREIGN KEY (`mar_keymar`) REFERENCES `marca` (`mar_keymar`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

