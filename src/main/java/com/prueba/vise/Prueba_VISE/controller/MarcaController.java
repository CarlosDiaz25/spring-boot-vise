package com.prueba.vise.Prueba_VISE.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prueba.vise.Prueba_VISE.model.Marca;
import com.prueba.vise.Prueba_VISE.servicio.MarcaServicio;

@Controller
public class MarcaController {
	
	@Autowired
	private MarcaServicio servicio;
	
	@GetMapping("/marcas")
	public String viewProductPage(Model model) 
	{
		try 
		{
			
			List<Marca> listMarcas = servicio.listAll();
			model.addAttribute("listMarcas", listMarcas);
			return "Marcas/index_marcas";
			
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	@RequestMapping("/marcas/new")
	public String showNewMarcaForm(Model model) 
	{
		try 
		{
		
			Marca marca = new Marca();
			model.addAttribute("marca",marca);
			return "Marcas/new_marca";
		
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
	@RequestMapping(value = "/saveMarca", method = RequestMethod.POST)
	public String saveMarca(@ModelAttribute("marca") Marca marca) 
	{
		try 
		{
			LocalDateTime myObj = LocalDateTime.now();
			if(marca.getMar_fecalt() == null)
				marca.setMar_fecalt(myObj);
			
			marca.setMar_fecult(myObj);
			servicio.save(marca);
		     
		    return "redirect:/marcas";
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
	@RequestMapping("/editMarca/{mar_keymar}")
	public ModelAndView showEditMarcaPage(@PathVariable(name = "mar_keymar") Long id)
	{
		try 
		{
			ModelAndView mav = new ModelAndView("Marcas/edit_marca");
		    Marca marca = servicio.get(id);
		    mav.addObject("marca", marca);
		     
		    return mav;
		} 
		catch (Exception e)
		{
			return null;
		}
	}
	
	@RequestMapping("/deleteMarca/{mar_keymar}")
	public String deleteMarca(@PathVariable(name = "mar_keymar") Long id) 
	{
		try 
		{
			servicio.delete(id);
		    return "redirect:/marcas"; 
		}
		    catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
}
