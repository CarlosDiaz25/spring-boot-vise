package com.prueba.vise.Prueba_VISE.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prueba.vise.Prueba_VISE.model.Marca;
import com.prueba.vise.Prueba_VISE.model.Producto;
import com.prueba.vise.Prueba_VISE.servicio.MarcaServicio;
import com.prueba.vise.Prueba_VISE.servicio.ProductoServicio;

@Controller
public class ProductoController {

	@Autowired
	private ProductoServicio servicio;
	
	@Autowired
	private MarcaServicio servicioMarca;
	
	@RequestMapping("/productos")
	public String viewProductPage(Model model) 
	{
		try 
		{
			
			List<Producto> listProductos = servicio.listAll();
			model.addAttribute("listProductos", listProductos);
			return "Productos/index";
			
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
	@RequestMapping("/productos/new")
	public String showNewProductForm(Model model) 
	{
		try 
		{
			List<Marca> listMarca = servicioMarca.listAll();
			Producto product = new Producto();
			
			model.addAttribute("product",product);
			model.addAttribute("listMarca", listMarca);
			return "Productos/new_producto";
		
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("product") Producto product) 
	{
		try 
		{
			LocalDateTime myObj = LocalDateTime.now();
			if(product.getPrd_fecalt() == null)
				product.setPrd_fecalt(myObj);
			
			product.setPrd_fecult(myObj);
			servicio.save(product);
		     
		    return "redirect:/productos";
		} 
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	
	@RequestMapping("/edit/{prd_keyprd}")
	public ModelAndView showEditProductPage(@PathVariable(name = "prd_keyprd") Long id,Model model)
	{
		try 
		{
			ModelAndView mav = new ModelAndView("Productos/edit_product");
		    Producto product = servicio.get(id);
		    mav.addObject("product", product);
		    List<Marca> listMarca = servicioMarca.listAll();
		    model.addAttribute("listMarca", listMarca);
		    return mav;
		} 
		catch (Exception e)
		{
			return null;
		}
	}
	
	@RequestMapping("/delete/{prd_keyprd}")
	public String deleteProduct(@PathVariable(name = "prd_keyprd") Long id) 
	{
		try 
		{
			servicio.delete(id);
		    return "redirect:/productos"; 
		}
		    catch (Exception e)
		{
			return e.getMessage();
		}
	}
}
