package com.prueba.vise.Prueba_VISE.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
	
	@GetMapping("")
	public String viewHomePage(Model model) {
		return "Dashboard";
	}
	
	
}
