package com.prueba.vise.Prueba_VISE.servicio;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.vise.Prueba_VISE.model.Marca;
import com.prueba.vise.Prueba_VISE.repositorio.IMarcaRepositorio;
@Service
public class MarcaServicio {
	@Autowired
	private IMarcaRepositorio repo;
	
	public List<Marca> listAll(){
		return repo.findAll();
	}
	
	public void save (Marca p){
		repo.save(p);
	}
	
	public Marca get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete (Long id) {	
		repo.deleteById(id);
	}
}
