package com.prueba.vise.Prueba_VISE.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.vise.Prueba_VISE.model.Producto;
import com.prueba.vise.Prueba_VISE.repositorio.IProductoRepositorio;

@Service
public class ProductoServicio {
	
	@Autowired
	private IProductoRepositorio repo;
	
	public List<Producto> listAll(){
		return repo.findAll();
	}
	
	public void save (Producto p){
		repo.save(p);
	}
	
	public Producto get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete (Long id) {	
		repo.deleteById(id);
	}
}
