package com.prueba.vise.Prueba_VISE.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Marca")
public class Marca {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long mar_keymar;
	
	@Column(length = 100,nullable = false,unique = true)
	private String mar_nombre;
	
	@Column(nullable = false)
	private LocalDateTime mar_fecalt;
	
	@Column(nullable = false)
	private LocalDateTime mar_fecult;

	public Marca() {
	}

	public Long getMar_keymar() {
		return mar_keymar;
	}

	public void setMar_keymar(Long mar_keymar) {
		this.mar_keymar = mar_keymar;
	}

	public String getMar_nombre() {
		return mar_nombre;
	}

	public void setMar_nombre(String mar_nombre) {
		this.mar_nombre = mar_nombre;
	}

	public LocalDateTime getMar_fecalt() {
		return mar_fecalt;
	}

	public void setMar_fecalt(LocalDateTime mar_fecalt) {
		this.mar_fecalt = mar_fecalt;
	}

	public LocalDateTime getMar_fecult() {
		return mar_fecult;
	}

	public void setMar_fecult(LocalDateTime mar_fecult) {
		this.mar_fecult = mar_fecult;
	}
	
}
