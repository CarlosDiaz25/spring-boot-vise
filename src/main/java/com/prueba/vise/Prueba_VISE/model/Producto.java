package com.prueba.vise.Prueba_VISE.model;


import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long prd_keyprd;

	@Column(length = 100,nullable = false,unique = true)
	private String prd_nombre;
	
	@ManyToOne
	@JoinColumn(name = "mar_keymar")
	private Marca marca;
	
	@Column(length = 100,nullable = false)
	private String prd_hechon;
	
	@Column(length = 100,nullable = false)
	private float prd_precio;
	
	@Column(nullable = false)
	private LocalDateTime prd_fecalt;
	
	@Column(nullable = false)
	private LocalDateTime prd_fecult;
	
	public Producto() {
	}
	
	
	public Long getPrd_keyprd() {
		return prd_keyprd;
	}
	public void setPrd_keyprd(Long prd_keyprd) {
		this.prd_keyprd = prd_keyprd;
	}
	public String getPrd_nombre() {
		return prd_nombre;
	}
	public void setPrd_nombre(String prd_nombre) {
		this.prd_nombre = prd_nombre;
	}
	public String getPrd_hechon() {
		return prd_hechon;
	}
	public void setPrd_hechon(String prd_hechon) {
		this.prd_hechon = prd_hechon;
	}
	public float getPrd_precio() {
		return prd_precio;
	}
	public void setPrd_precio(float prd_precio) {
		this.prd_precio = prd_precio;
	}
	
	public LocalDateTime getPrd_fecalt() {
		return prd_fecalt;
	}
	public void setPrd_fecalt(LocalDateTime prd_fecalt) {
		this.prd_fecalt = prd_fecalt;
	}
	
	public LocalDateTime getPrd_fecult() {
		return prd_fecult;
	}
	public void setPrd_fecult(LocalDateTime prd_fecult) {
		this.prd_fecult = prd_fecult;
	}


	public Marca getMarca() {
		return marca;
	}


	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	
}
