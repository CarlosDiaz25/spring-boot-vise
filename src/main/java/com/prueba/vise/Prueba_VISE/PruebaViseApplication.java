package com.prueba.vise.Prueba_VISE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaViseApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaViseApplication.class, args);
	}

}
