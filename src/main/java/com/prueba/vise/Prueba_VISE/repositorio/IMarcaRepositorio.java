package com.prueba.vise.Prueba_VISE.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.vise.Prueba_VISE.model.Marca;

public interface IMarcaRepositorio extends JpaRepository<Marca, Long> {

}
