package com.prueba.vise.Prueba_VISE.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.vise.Prueba_VISE.model.Producto;

public interface IProductoRepositorio extends JpaRepository<Producto, Long> {

}
